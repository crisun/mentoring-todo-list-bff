package com.mentoring.todolist.exception

import io.ktor.http.HttpStatusCode

open class BaseHttpException(message: String, val httpStatus: Int, throwable: Throwable?) : Exception(message, throwable)

class ValidationException(message: String) : BaseHttpException(message, HttpStatusCode.BadRequest.value, null)

class NotFoundException(message: String) : BaseHttpException(message, HttpStatusCode.NotFound.value, null)

class DatabaseException(message: String) : BaseHttpException(message, HttpStatusCode.InternalServerError.value, null)
