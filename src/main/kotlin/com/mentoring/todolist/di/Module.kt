package com.mentoring.todolist.di

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.mentoring.todolist.configuration.AuthConfig
import com.mentoring.todolist.configuration.Configuration
import com.mentoring.todolist.configuration.DatabaseConfig
import com.mentoring.todolist.repository.TaskRepository
import com.mentoring.todolist.repository.UserRepository
import com.mentoring.todolist.service.TaskService
import com.mentoring.todolist.service.UserService
import com.mentoring.todolist.util.Md5Sum
import io.ktor.util.KtorExperimentalAPI
import org.koin.dsl.module
import org.koin.experimental.builder.single
import org.springframework.jdbc.core.JdbcTemplate
import java.security.MessageDigest
import java.util.*
import javax.sql.DataSource

@KtorExperimentalAPI
val appModule = module {
    single<TaskService>()
    single<UserService>()
    single<Configuration>()
    single<TaskRepository>()
    single<UserRepository>()

    single { JdbcTemplate(get()) }
    single<DataSource> { DatabaseConfig(get()).dataSource() }
    single { Md5Sum(MessageDigest.getInstance("MD5")) }
}


@KtorExperimentalAPI
val authModule = module {
    single<AuthConfig>()
}

val jsonModule = module {
    single {
        jacksonObjectMapper().apply {
            configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
            disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
            setSerializationInclusion(JsonInclude.Include.NON_NULL)
            setSerializationInclusion(JsonInclude.Include.NON_EMPTY)
            setTimeZone(TimeZone.getTimeZone("America/Sao_Paulo"))
            registerModule(JavaTimeModule())
        }
    }
}
