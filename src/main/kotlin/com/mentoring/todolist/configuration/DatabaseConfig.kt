package com.mentoring.todolist.configuration

import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import io.ktor.util.KtorExperimentalAPI

@KtorExperimentalAPI
class DatabaseConfig(private val config: Configuration) {
    fun dataSource(): HikariDataSource {
        return HikariDataSource(hikariConfig())
    }

    private fun hikariConfig() = HikariConfig().apply {
        config.run {
            isAutoCommit = true
            jdbcUrl = fetchJdbcUrl()
            poolName = fetchPoolName()
            minimumIdle = fetchMinIdle()
            maximumPoolSize = fetchMaxPoolSize()
            driverClassName = fetchDriverClassName()
            connectionTestQuery = fetchConnectionTestQuery()
            transactionIsolation = "TRANSACTION_REPEATABLE_READ"
        }
        validate()
    }
}
