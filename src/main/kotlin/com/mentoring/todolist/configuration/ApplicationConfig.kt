package com.mentoring.todolist.configuration

import com.fasterxml.jackson.databind.ObjectMapper
import com.mentoring.todolist.di.appModule
import com.mentoring.todolist.di.authModule
import com.mentoring.todolist.di.jsonModule
import com.mentoring.todolist.exception.BaseHttpException
import com.mentoring.todolist.routes.auth
import com.mentoring.todolist.routes.health
import com.mentoring.todolist.routes.todo
import com.viartemev.ktor.flyway.FlywayFeature
import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.auth.Authentication
import io.ktor.auth.authentication
import io.ktor.auth.jwt.JWTPrincipal
import io.ktor.auth.jwt.jwt
import io.ktor.features.*
import io.ktor.http.ContentType
import io.ktor.http.HttpHeaders
import io.ktor.http.HttpMethod
import io.ktor.http.HttpStatusCode
import io.ktor.jackson.JacksonConverter
import io.ktor.jackson.jackson
import io.ktor.response.respond
import io.ktor.routing.routing
import io.ktor.util.KtorExperimentalAPI
import org.koin.ktor.ext.Koin
import org.koin.ktor.ext.inject
import org.slf4j.event.Level
import java.time.ZonedDateTime
import javax.sql.DataSource

@KtorExperimentalAPI
fun Application.module() {
    val jwt by inject<AuthConfig>()
    val dataSource by inject<DataSource>()
    val jacksonMapper by inject<ObjectMapper>()

    install(Compression)

    install(CallLogging) { level = Level.INFO }

    install(DefaultHeaders) {
        header("X-Developer", "crisun")
    }

    install(Koin) {
        modules(
            listOf(appModule, authModule, jsonModule)
        )
    }

    install(FlywayFeature) {
        this.dataSource = dataSource
    }

    install(ContentNegotiation) {
        jackson {
            register(ContentType.Application.Json, JacksonConverter(jacksonMapper))
        }
    }

    install(StatusPages) {
        exception<Throwable> { cause ->
            call.respond(HttpStatusCode.InternalServerError, "Internal Server Error")
            throw cause
        }
        exception<BaseHttpException> {
            call.respond(
                HttpStatusCode.fromValue(it.httpStatus),
                mapOf(
                    "status" to it.httpStatus,
                    "message" to (it.message ?: ""),
                    "timestamp" to ZonedDateTime.now().toOffsetDateTime()
                )
            )
        }
    }

    install(CORS) {
        anyHost()
        allowCredentials = true
        allowNonSimpleContentTypes = true
        method(HttpMethod.Get)
        method(HttpMethod.Post)
        method(HttpMethod.Put)
        method(HttpMethod.Delete)
        method(HttpMethod.Patch)
        method(HttpMethod.Options)
        header(HttpHeaders.ContentType)
        header(HttpHeaders.AccessControlAllowHeaders)
        header(HttpHeaders.AccessControlAllowOrigin)
    }

    authentication {
        jwt {
            realm = "todo-list-bff"

            verifier(jwt.verifier)

            validate { credentials ->
                if (credentials.payload.claims.contains("user")) {
                    JWTPrincipal(credentials.payload)
                } else {
                    null
                }
            }
        }
    }

    routing {
        auth()
        todo()
        health()
    }
}
