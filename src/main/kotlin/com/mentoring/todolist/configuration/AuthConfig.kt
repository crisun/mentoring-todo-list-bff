package com.mentoring.todolist.configuration

import com.auth0.jwt.JWT
import com.auth0.jwt.JWTVerifier
import com.auth0.jwt.algorithms.Algorithm
import com.fasterxml.jackson.databind.ObjectMapper
import com.mentoring.todolist.model.User
import io.ktor.util.KtorExperimentalAPI
import java.util.*

@KtorExperimentalAPI
open class AuthConfig(private val mapper: ObjectMapper, private val config: Configuration) {
    private val validityInMs = 3_600_000 * 2
    private val algorithm = Algorithm.HMAC256("config")

    val verifier: JWTVerifier = JWT.require(algorithm)
        .withIssuer(config.fetchJwtIssuer())
        .withAudience(config.fetchJwtAudience())
        .build()

    fun sign(user: User): String = JWT.create()
        .withExpiresAt(expiresAt())
        .withSubject(user.id.toString())
        .withIssuer(config.fetchJwtIssuer())
        .withAudience(config.fetchJwtAudience())
        .withClaim("user", mapper.writeValueAsString(user.copy(password = "")))
        .sign(algorithm)

    private fun expiresAt() = Date(System.currentTimeMillis() + validityInMs)
}
