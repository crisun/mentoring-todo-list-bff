package com.mentoring.todolist.configuration

import com.typesafe.config.ConfigFactory
import io.ktor.config.HoconApplicationConfig
import io.ktor.util.KtorExperimentalAPI

@KtorExperimentalAPI
class Configuration {
    companion object {
        private const val MIN_IDLE_KEY = "env.datasource.minIdle"
        private const val JDBC_URL_KEY = "env.datasource.jdbcUrl"
        private const val PASSWORD_KEY = "env.datasource.password"
        private const val USERNAME_KEY = "env.datasource.username"
        private const val POOL_NAME_KEY = "env.datasource.poolName"
        private const val MAX_POOL_SIZE_KEY = "env.datasource.maxPoolSize"
        private const val DRIVER_CLASSNAME_KEY = "env.datasource.driverClassName"
        private const val CONNECTION_TEST_QUERY_KEY = "env.datasource.connectionTestQuery"

        private const val JWT_ISSUER_KEY = "env.jwt.issuer"
        private const val JWT_AUDIENCE_KEY = "env.jwt.audience"
    }

    private val config by lazy {
        HoconApplicationConfig(ConfigFactory.load())
    }

    fun fetchJdbcUrl() = fetchProperty(JDBC_URL_KEY)

    fun fetchPassword() = fetchProperty(PASSWORD_KEY)

    fun fetchUsername() = fetchProperty(USERNAME_KEY)

    fun fetchPoolName() = fetchProperty(POOL_NAME_KEY)

    fun fetchMinIdle() = fetchProperty(MIN_IDLE_KEY).toInt()

    fun fetchDriverClassName() = fetchProperty(DRIVER_CLASSNAME_KEY)

    fun fetchMaxPoolSize() = fetchProperty(MAX_POOL_SIZE_KEY).toInt()

    fun fetchConnectionTestQuery() = fetchProperty(CONNECTION_TEST_QUERY_KEY)

    fun fetchJwtIssuer() = fetchProperty(JWT_ISSUER_KEY)

    fun fetchJwtAudience() = fetchProperty(JWT_AUDIENCE_KEY)

    private fun fetchProperty(key: String) = config.property(key).getString()
}
