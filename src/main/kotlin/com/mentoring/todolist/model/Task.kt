package com.mentoring.todolist.model

import java.time.LocalDateTime

data class Task(
    val id: Long,
    val title: String,
    val description: String,
    val createDate: LocalDateTime = LocalDateTime.now(),
    val startDate: LocalDateTime? = null,
    val finishDate: LocalDateTime? = null,
    val status: Status = Status.CREATED
)
