package com.mentoring.todolist.model

enum class Status {
    CREATED, STARTED, FINISHED
}
