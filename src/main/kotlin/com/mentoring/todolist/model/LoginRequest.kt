package com.mentoring.todolist.model

data class LoginRequest(val username: String, val password: String)
