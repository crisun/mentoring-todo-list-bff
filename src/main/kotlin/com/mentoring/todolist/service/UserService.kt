package com.mentoring.todolist.service

import com.mentoring.todolist.exception.NotFoundException
import com.mentoring.todolist.model.LoginRequest
import com.mentoring.todolist.model.User
import com.mentoring.todolist.repository.UserRepository
import com.mentoring.todolist.util.Md5Sum

class UserService(private val md5: Md5Sum, private val repository: UserRepository) {
    fun findUser(login: LoginRequest): User {
        return repository.findUser(login.username, md5.checksum(login.password)) ?: throw NotFoundException("User not found")
    }
}
