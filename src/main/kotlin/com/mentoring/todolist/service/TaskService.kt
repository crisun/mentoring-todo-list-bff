package com.mentoring.todolist.service

import com.mentoring.todolist.exception.NotFoundException
import com.mentoring.todolist.model.Task
import com.mentoring.todolist.repository.TaskRepository

class TaskService(private val repository: TaskRepository) {
    fun list(): List<Task> {
        return repository.list()
    }

    fun insert(task: Task): Task {
        return repository.insert(task)
    }

    fun update(id: Int, task: Task) {
        if (repository.update(id, task) == 0) {
            throw NotFoundException("Task not found")
        }
    }

    fun delete(id: Int) {
        repository.delete(id)
    }

    fun findById(id: Int): Task {
        return repository.findById(id) ?: throw NotFoundException("Task not found")
    }
}
