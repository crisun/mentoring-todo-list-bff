package com.mentoring.todolist.routes

import com.mentoring.todolist.exception.ValidationException
import com.mentoring.todolist.model.Task
import com.mentoring.todolist.service.TaskService
import io.ktor.application.call
import io.ktor.auth.authenticate
import io.ktor.http.HttpStatusCode
import io.ktor.request.receive
import io.ktor.request.receiveOrNull
import io.ktor.response.respond
import io.ktor.routing.*
import org.koin.ktor.ext.inject

fun Routing.todo() {
    val service by inject<TaskService>()

    authenticate {
        get("/todos") {
            call.respond(service.list())
        }
    }

    authenticate {
        get("/todo/{id}") {
            val id = call.parameters["id"] ?: throw ValidationException("Id parameter can't be null")

            call.respond(service.findById(id.toInt()))
        }
    }

    authenticate {
        post("/todo") {
            call.respond(HttpStatusCode.Created, service.insert(call.receive()))
        }
    }

    authenticate {
        put("/todo/{id}") {
            val id = call.parameters["id"] ?: throw ValidationException("Id parameter can't be null")
            val task = call.receiveOrNull<Task>() ?: throw ValidationException("Task parameter can't be null")

            service.update(id.toInt(), task).let {
                call.respond(HttpStatusCode.OK)
            }
        }
    }

    authenticate {
        delete("/todo/{id}") {
            val id = call.parameters["id"] ?: throw ValidationException("Id parameter can't be null")

            service.delete(id.toInt()).let {
                call.respond(HttpStatusCode.OK)
            }
        }
    }
}
